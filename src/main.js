import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;

new Vue({
  router,
  store, // Vuex
  render: h => h(App) //render component ตัวแรกมาแสดง ex. เรียกไฟล์ App.vue มาแสดง
}).$mount("#app"); // id="app" ในไฟล์ index.html 
